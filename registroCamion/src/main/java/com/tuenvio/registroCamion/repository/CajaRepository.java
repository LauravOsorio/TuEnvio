package com.tuenvio.registroCamion.repository;

import com.tuenvio.registroCamion.model.Caja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CajaRepository extends JpaRepository<Caja, String>{

}
