package com.tuenvio.registroCamion.service;

import com.tuenvio.registroCamion.model.Camion;

import java.util.List;

public interface CamionService {

    Camion registrarCamion(Camion camion);

    List<Camion> findByTipoProducto(String tipoProducto);

    List<Camion> actualizarCamiones(List<Camion> camion);

}
