package com.tuenvio.registroCamion.service;

import com.tuenvio.registroCamion.model.Caja;

import java.util.List;

public interface CajaService {
    List<Caja> registrarCajas(List<Caja> caja);
}
