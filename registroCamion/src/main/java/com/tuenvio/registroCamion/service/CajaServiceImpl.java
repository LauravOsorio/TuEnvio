package com.tuenvio.registroCamion.service;

import com.tuenvio.registroCamion.model.Caja;
import com.tuenvio.registroCamion.repository.CajaRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CajaServiceImpl implements CajaService{
    @Autowired
    private CajaRepository cajaRepository;

    @Override
    public List<Caja> registrarCajas(List<Caja> caja) {
        return cajaRepository.saveAll(caja);
    }
}
