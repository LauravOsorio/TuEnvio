package com.tuenvio.registroCamion.service;

import com.tuenvio.registroCamion.model.Camion;
import com.tuenvio.registroCamion.repository.CamionRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Creado por Laura Osorio 17/01/2023
 */
@Service
@AllArgsConstructor
public class CamionServiceImpl implements CamionService{
    @Autowired
    private CamionRepository camionRepository;

    /**
     * metodo que permite almacenar un camion
     */
    @Override
    public Camion registrarCamion(Camion camion) {
        return camionRepository.save(camion);
    }

    @Override
    public List<Camion> findByTipoProducto(String tipoProducto) {
        return camionRepository.findByTipoProducto(tipoProducto);
    }
    @Override
    public List<Camion> actualizarCamiones(List<Camion> camion){
        return camionRepository.saveAll(camion);
    }


}
