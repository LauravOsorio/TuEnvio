package com.tuenvio.registroCamion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistroCamionApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistroCamionApplication.class, args);
	}

}
