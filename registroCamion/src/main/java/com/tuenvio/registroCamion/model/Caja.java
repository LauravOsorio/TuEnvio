package com.tuenvio.registroCamion.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="caja")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Caja {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tipoProducto;
    private int cantidad;
    @ManyToOne
    private Camion camion;
}
