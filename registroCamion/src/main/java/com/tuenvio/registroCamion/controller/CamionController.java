package com.tuenvio.registroCamion.controller;

import com.tuenvio.registroCamion.model.Camion;
import com.tuenvio.registroCamion.service.CamionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/camiones")
public class CamionController {
    @Autowired
    private CamionService camionService;

    @PostMapping
    public ResponseEntity<Camion> registrarCamion(@RequestBody Camion camion) {

        Camion camionRegistrado = camionService.registrarCamion(camion);
        return new ResponseEntity<Camion>(camionRegistrado, HttpStatus.CREATED);
    }
}
