package com.tuenvio.registroCamion.controller;

import com.tuenvio.registroCamion.model.Caja;
import com.tuenvio.registroCamion.model.Camion;
import com.tuenvio.registroCamion.service.CajaService;
import com.tuenvio.registroCamion.service.CamionService;
import org.aspectj.bridge.IMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cajas")
public class CajaController {

    @Autowired
    private CajaService cajaService;

    @Autowired
    private CamionService camionService;

    @PostMapping
    public ResponseEntity<Caja> registrarCajas(@RequestBody Caja caja) {
        List<Camion> camiones = camionService.findByTipoProducto(caja.getTipoProducto());

        if(camiones.isEmpty()){
            return new ResponseEntity("El tipo de producto de la caja no coincide con el del camión", HttpStatus.BAD_REQUEST);
        }

        int capacidadTotal = 0;
        for(Camion c:camiones){
            capacidadTotal = c.getCapacidad() + capacidadTotal;
        }

        if(caja.getCantidad() > capacidadTotal){
            return new ResponseEntity("La cantidad de cajas, excede la capacidad maxima de los camiones para este tipo de producto",HttpStatus.BAD_REQUEST);
        }

        List<Caja> cajas = new ArrayList<Caja>();
        int cantidad = caja.getCantidad();
        for(Camion c:camiones){
            int capacidadCamion = c.getCapacidad();
            for(int i=0; i < capacidadCamion; i++){
                Caja cajaNueva = new Caja();
                cajaNueva.setCamion(c);
                cajaNueva.setTipoProducto(caja.getTipoProducto());
                cajaNueva.setCantidad(caja.getCantidad());
                cajas.add(cajaNueva);
                c.setCapacidad(c.getCapacidad()-1);

                if(cajas.size() == cantidad){
                    break;
                }
            }
            if(cajas.size() == cantidad){
                break;
            }
        }
        camionService.actualizarCamiones(camiones);

        List<Caja> nuevasCajas = cajaService.registrarCajas(cajas);
        return new ResponseEntity(nuevasCajas, HttpStatus.CREATED);
    }
}
